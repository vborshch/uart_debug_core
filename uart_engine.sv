
//
// Project       : UART core
// Author        : Borshch Vladislav
// Contacts      : borchsh.vn@gmail.com
// Workfile      : uart_engine.sv
// Description   : UART core top module
//

/*
  logic                      uart__iclk            ;
  logic                      uart__iclk_ena        ;
  logic                      uart__irst            ;
  logic                      uart__ireq            ;
  logic                      uart__irx             ;
  logic                      uart__otx             ;
  logic                      uart__iena            ;
  logic    [pUART_W_DAT-1:0] uart__idat            ;
  logic                      uart__icena           ;
  logic    [pUART_W_DAT-1:0] uart__icdat           ;
  logic                      uart__oena            ;
  logic                [7:0] uart__odat            ;

  assign uart__iclk            = ;
  assign uart__iclk_ena        = ;
  assign uart__irst            = ;
  assign uart__ireq            = ;
  assign uart__irx             = ;
  assign uart__iena            = ;
  assign uart__idat            = ;
  assign uart__icena           = ;
  assign uart__icdat           = ;

  uart_engine
  #(
    . pW_DAT                         (pUART_W_DAT                         ) ,
    . pW_ADR                         (pUART_W_ADR                         ) 
  )
  uart__
  (
    . iclk                           (uart__iclk                          ) ,
    . iclk_ena                       (uart__iclk_ena                      ) ,
    . irst                           (uart__irst                          ) ,
    . ireq                           (uart__ireq                          ) ,
    . irx                            (uart__irx                           ) ,
    . otx                            (uart__otx                           ) ,
    . iena                           (uart__iena                          ) ,
    . icena                          (uart__icena                         ) ,
    . icdat                          (uart__icdat                         ) ,
    . idat                           (uart__idat                          ) ,
    . oena                           (uart__oena                          ) ,
    . odat                           (uart__odat                          )
  );
*/
module uart_engine
  #(
    parameter int pW_DAT = 24  ,
    parameter int pW_ADR = 3   ,
    parameter int pT_CLK = "ext"
  )
  (
    input                iclk     , // Internal clock
    input                iclk_ena , // UART clk_ena
    input                irst     ,
    // UART
    input                irx      , // UART RX line
    output logic         otx      , // UART TX line
    // Data
    input                ireq     ,
    input                iena     ,
    input   [pW_DAT-1:0] idat     ,
    // Control
    input                icena    ,
    input   [pW_DAT-1:0] icdat    ,
    output logic         oena     ,
    output logic   [7:0] odat
  );

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------
  
  logic              tx__iclk    ;
  logic              tx__iclk_ena;
  logic              tx__irst    ;
  logic              tx__iena    ;
  logic [pW_DAT-1:0] tx__idat    ;
  logic              tx__oreq    ;
  logic              tx__otx     ;

  logic              rx__iclk    ;
  logic              rx__irst    ;
  logic              rx__irx     ;
  logic              rx__oena    ;
  logic       [7:0]  rx__odat    ;  

  logic              buf__irst   ;
  logic              buf__iclk   ;
  logic              buf__iena   ;
  logic [pW_DAT-1:0] buf__idat   ;
  logic              buf__ireq   ;
  logic              buf__oena   ;
  logic [pW_DAT-1:0] buf__odat   ;
  logic              buf__oempty ;
  logic              buf__ofull  ;

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  logic         rdy;

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  // Check buffer
  always_ff@(posedge iclk) begin
    if (buf__ofull)        rdy <= 1'b1;
    else if (buf__oempty)  rdy <= 1'b0;
  end

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    otx  <= tx__otx;
    oena <= rx__oena;
    odat <= rx__odat;
  end

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------
  
  assign tx__iclk     = iclk;
  assign tx__iclk_ena = iclk_ena;
  assign tx__irst     = irst;
  assign tx__iena     = (buf__oena) ? (1'b1     ) : (icena);
  assign tx__idat     = (buf__oena) ? (buf__odat) : (icdat);

  assign rx__iclk     = iclk;
  assign rx__irst     = irst;
  assign rx__irx      = irx;

  assign buf__irst    = irst | ireq;
  assign buf__iclk    = iclk;
  assign buf__iena    = iena & ~rdy;
  assign buf__idat    = idat;
  assign buf__ireq    = tx__oreq & rdy;

  uart_tx
  #(
    . pW_DAT    (pW_DAT               )
  )
  tx__
  (
    . iclk      (tx__iclk             ) ,
    . iclk_ena  (tx__iclk_ena         ) ,
    . irst      (tx__irst             ) ,
    . iena      (tx__iena             ) ,
    . idat      (tx__idat             ) ,
    . oreq      (tx__oreq             ) ,
    . otx       (tx__otx              )
  );

  uart_rx
  #(
    . pT_CLK    (pT_CLK               )
  )
  rx__
  (
    . iclk      (rx__iclk             ) ,
    . irst      (rx__irst             ) ,
    . irx       (rx__irx              ) ,
    . oena      (rx__oena             ) ,
    . odat      (rx__odat             )
  );

  uart_buffer
  #(
    . pW_ADR    (pW_ADR               ) ,
    . pW_DAT    (pW_DAT               )
  )
  buf__
  (
    . irst      (buf__irst            ) ,
    . iclk      (buf__iclk            ) ,
    . iena      (buf__iena            ) ,
    . idat      (buf__idat            ) ,
    . ireq      (buf__ireq            ) ,
    . oena      (buf__oena            ) ,
    . odat      (buf__odat            ) ,
    . oempty    (buf__oempty          ) ,
    . ofull     (buf__ofull           ) 
  );


endmodule